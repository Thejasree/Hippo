package org.example.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import java.util.Calendar;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.onehippo.cms7.essentials.components.rest.adapters.HippoGalleryImageAdapter;

@XmlRootElement(name = "dealdocfour")
@XmlAccessorType(XmlAccessType.NONE)
@HippoEssentialsGenerated(internalName = "myhippoproject:DealDocFour")
@Node(jcrType = "myhippoproject:DealDocFour")
public class DealDocFour extends BaseDocument {
    @XmlElement
    @HippoEssentialsGenerated(internalName = "myhippoproject:company")
    public String getCompany() {
        return getProperty("myhippoproject:company");
    }

    @XmlElement
    @HippoEssentialsGenerated(internalName = "myhippoproject:website")
    public String getWebsite() {
        return getProperty("myhippoproject:website");
    }

    @XmlElement
    @HippoEssentialsGenerated(internalName = "myhippoproject:location")
    public String getLocation() {
        return getProperty("myhippoproject:location");
    }

    @XmlElement
    @HippoEssentialsGenerated(internalName = "myhippoproject:industry")
    public String getIndustry() {
        return getProperty("myhippoproject:industry");
    }

    @XmlElement
    @HippoEssentialsGenerated(internalName = "myhippoproject:description")
    public String getDescription() {
        return getProperty("myhippoproject:description");
    }

    @XmlElement
    @HippoEssentialsGenerated(internalName = "myhippoproject:required fund")
    public Double getRequiredfund() {
        return getProperty("myhippoproject:required fund");
    }

    @XmlElement
    @HippoEssentialsGenerated(internalName = "myhippoproject:minimum investment")
    public Double getMinimuminvestment() {
        return getProperty("myhippoproject:minimum investment");
    }

    @XmlElement
    @HippoEssentialsGenerated(internalName = "myhippoproject:founded")
    public Long getFounded() {
        return getProperty("myhippoproject:founded");
    }

    @XmlElement
    @HippoEssentialsGenerated(internalName = "myhippoproject:employees")
    public Long getEmployees() {
        return getProperty("myhippoproject:employees");
    }

    @XmlElement
    @HippoEssentialsGenerated(internalName = "myhippoproject:type")
    public String getType() {
        return getProperty("myhippoproject:type");
    }

    @XmlElement
    @HippoEssentialsGenerated(internalName = "myhippoproject:raising")
    public Double getRaising() {
        return getProperty("myhippoproject:raising");
    }

    @XmlElement
    @HippoEssentialsGenerated(internalName = "myhippoproject:per")
    public Double getPer() {
        return getProperty("myhippoproject:per");
    }

    @XmlElement
    @HippoEssentialsGenerated(internalName = "myhippoproject:annual revenue")
    public Double getAnnualrevenue() {
        return getProperty("myhippoproject:annual revenue");
    }

    @XmlElement
    @HippoEssentialsGenerated(internalName = "myhippoproject:previous capital")
    public Double getPreviouscapital() {
        return getProperty("myhippoproject:previous capital");
    }

    @XmlElement
    @HippoEssentialsGenerated(internalName = "myhippoproject:end date")
    public Calendar getEnddate() {
        return getProperty("myhippoproject:end date");
    }

    @XmlElement
    @HippoEssentialsGenerated(internalName = "myhippoproject:percent of investment")
    public Long getPercentofinvestment() {
        return getProperty("myhippoproject:percent of investment");
    }

    @XmlJavaTypeAdapter(HippoGalleryImageAdapter.class)
    @XmlElement
    @HippoEssentialsGenerated(internalName = "myhippoproject:logo")
    public HippoGalleryImageSet getLogo() {
        return getLinkedBean("myhippoproject:logo", HippoGalleryImageSet.class);
    }

    @XmlElement
    @HippoEssentialsGenerated(internalName = "hippotranslation:id")
    public String getId() {
        return getProperty("hippotranslation:id");
    }

    @XmlElement
    @HippoEssentialsGenerated(internalName = "hippostdpubwf:creationDate")
    public Calendar getcreationDate() {
        return getProperty("hippostdpubwf:creationDate");
    }

    @XmlElement
    @HippoEssentialsGenerated(internalName = "hippostdpubwf:lastModificationDate")
    public Calendar getlastModificationDate() {
        return getProperty("hippostdpubwf:lastModificationDate");
    }
}
